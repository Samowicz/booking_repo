import http.client
import urllib.request
import urllib.parse
import urllib.error
import json
import os
import ssl


if not os.environ.get('PYTHONHTTPSVERIFY', '') and getattr(ssl, '_create_unverified_context', None):
    ssl._create_default_https_context = ssl._create_unverified_context

INBOUND = 'inboundFlight'
OUTBOUND = 'outboundFlight'
DEPARTURE = 'departureAirport'
LOCATION_CODE = 'locationCode'
ARRIVAL = 'arrivalAirport'
DEPARTURE_TIME = 'departureDateTime'
ID = 'id'
PRICING = 'pricingInfo'
TOTAL_PRICE = 'totalPriceAllPassengers'
CURRENCY = 'currencyCode'


class Flight:
    """create a Flight class with the following parameters"""
    def __init__(self):
        self.id_outbound = ""
        self.id_inbound = ""
        self.origin_outbound = ""
        self.destination_outbound = ""
        self.flight_date_outbound = ""
        self.flight_date_inbound = ""
        self.price = ""
        self.currency = ""


class FlightParser:

    def __init__(self):
        self.api_response = {}
        self.flights = []

    def fetch_response(self, parameters):
        """fetch the API response and return a JSON of the response"""

        headers = {
            # Request headers
            'X-Deeplink-CultureCode': 'nl-NL',
            'apikey': '13a0c897c2e24b90a0d8a5fdf1578c8e',
        }

        params = urllib.parse.urlencode(parameters)

        try:
            conn = http.client.HTTPSConnection('api.transavia.com')
            conn.request("GET", "/v1/flightoffers/?%s" % params, "{body}", headers)
            response = conn.getresponse()
            data = response.read()
            self.api_response = json.loads(data)
            conn.close()
        except Exception as e:
            print("[Errno {0}] {1}".format(e.errno, e.strerror))

    def fetch_flights(self):
        """create a list of all possibilities of flight for a fixed date"""
        roundtrips = self.api_response['flightOffer']
        for roundtrip in roundtrips:
            flightparser = FlightFetchInfo(roundtrip)
            flight = flightparser.fetch_flight_data()
            self.flights.append(flight)
        return self.flights

    def flight_possibilities(self):
        """return the number of flight possibilities"""
        return int(self.api_response['resultSet']['count'])


class FlightFetchInfo:

    def __init__(self, flightblock):
        """This class gets a block as an argument and fetch each information on the given block"""
        self.flightblock = flightblock

    def fetch_flight_data(self):
        flight = Flight()
        outbound = self.flightblock[OUTBOUND]
        inbound = self.flightblock[INBOUND]
        flight.id_outbound = outbound[ID]
        flight.origin_outbound = outbound[DEPARTURE][LOCATION_CODE]
        flight.destination_outbound = outbound[ARRIVAL][LOCATION_CODE]
        flight.flight_date_outbound = outbound[DEPARTURE_TIME]
        flight.flight_date_inbound = inbound[DEPARTURE_TIME]
        flight.id_inbound = inbound[ID]
        flight.price = outbound[PRICING][TOTAL_PRICE] + inbound[PRICING][TOTAL_PRICE]
        flight.currency = inbound[PRICING][CURRENCY]
        return flight
