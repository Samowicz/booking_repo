import pymysql

USERNAME = 'root'
LOCAL_HOST = 'localhost'

class Database:
    """This class creates a database and insert data into it. Takes hotels and path to db in arguments"""
    def __init__(self, flight, db_name, mysql_password):
        self.flight = flight
        self.db_name = db_name
        self.mysql_password = mysql_password


    def create_table(self):
        con = pymysql.connect(host=LOCAL_HOST, user=USERNAME, password=self.mysql_password, database=self.db_name)
        cur = con.cursor()
        try:
            cur.execute('''CREATE TABLE flights (
                                        id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT, 
                                        id_outbound VARCHAR(200) DEFAULT NULL,
                                        id_inbound VARCHAR(200) DEFAULT NULL,
                                        origin VARCHAR(200) DEFAULT NULL,
                                        destination VARCHAR(200) DEFAULT NULL,
                                        flight_date_outbound DateTime,
                                        flight_date_inbound DateTime,
                                        price FLOAT DEFAULT NULL,
                                        currency VARCHAR(200) DEFAULT "EUR",
                                        timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP)
                                        ''')
            con.commit()
        except pymysql.InternalError:
            pass

        cur.close()


    def insert(self):
        """insert the data into the db"""
        con = pymysql.connect(host=LOCAL_HOST, user=USERNAME, password=self.mysql_password, database=self.db_name)
        cur = con.cursor()
        for i, flight in enumerate(self.flight):
            try:
                cur.execute(
                    '''INSERT INTO flights (id_outbound, id_inbound, origin, destination, flight_date_outbound, 
                    flight_date_inbound, price, currency ) VALUES (%s,%s,%s,%s,%s,%s,%s,%s)''',
                    (
                        flight.id_outbound,
                        flight.id_inbound,
                        flight.origin_outbound,
                        flight.destination_outbound,
                        flight.flight_date_outbound,
                        flight.flight_date_inbound,
                        flight.price,
                        flight.currency,
                    ))
                con.commit()
            except pymysql.IntegrityError:
                con.rollback()
            cur.close()


