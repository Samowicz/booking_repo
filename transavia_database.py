import sqlite3


class Database:
    """This class creates a database and insert data into it. Takes hotels and path to db in arguments"""
    def __init__(self, flight, db_path):
        self.flight = flight
        self.db_path = db_path


    def create(self):
        with sqlite3.connect(self.db_path) as con:
            cur = con.cursor()
            cur.execute('''CREATE TABLE flights (
                                        id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, 
                                        id_outbound VARCHAR DEFAULT NULL,
                                        id_inbound VARCHAR DEFAULT NULL,
                                        origin VARCHAR DEFAULT NULL,
                                        destination VARCHAR DEFAULT NULL,
                                        flight_date_outbound TIMESTAMP DEFAULT NULL,
                                        flight_date_inbound TIMESTAMP DEFAULT NULL,
                                        price FLOAT DEFAULT NULL,
                                        currency VARCHAR DEFAULT "EUR" )
                                        ''')
            con.commit()
            cur.close()


    def insert(self):
        """insert the data into the db"""
        with sqlite3.connect(self.db_path) as con:
            cur = con.cursor()
            for i, flight in enumerate(self.flight):
                try:
                    cur.execute(
                        '''INSERT INTO flights (id_outbound, id_inbound, origin, destination, flight_date_outbound, 
                        flight_date_inbound, price, currency ) VALUES (?,?,?,?,?,?,?,?)''',
                        [
                            flight.id_outbound,
                            flight.id_inbound,
                            flight.origin_outbound,
                            flight.destination_outbound,
                            flight.flight_date_outbound,
                            flight.flight_date_inbound,
                            flight.price,
                            flight.currency,
                        ])
                    con.commit()
                except sqlite3.IntegrityError:
                    con.rollback()
