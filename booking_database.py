import sqlite3


class Database:
    """This class creates a database and insert data into it. Takes hotels and path to db in arguments"""
    def __init__(self, hotels, db_path):
        self.hotels = hotels
        self.db_path = db_path

    def create(self):
        """create the database. If already exists, remove it and recreate it"""
        with sqlite3.connect(self.db_path) as con:
            cur = con.cursor()
            cur.execute('''CREATE TABLE hotel_metadata (
                                        id INT PRIMARY KEY, 
                                        name VARCHAR,
                                        dist_from_center INT DEFAULT NULL)''')
            cur.execute('''CREATE TABLE pricing (
                                        id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, 
                                        hotel_id INT,
                                        price VARCHAR DEFAULT NULL,
                                        score FLOAT DEFAULT NULL,
                                        experience INT DEFAULT NULL,
                                        date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
                                        FOREIGN KEY (hotel_id) REFERENCES hotel_metadata(name)
                                        )''')
            con.commit()
            cur.close()

    def insert_column(self, table, column):
        with sqlite3.connect(self.db_path) as con:
            cur = con.cursor()
            try:
                cur.execute(f'''ALTER TABLE {table} ADD COLUMN '{column}' ''')
            except:
                pass
            cur.close()

    def insert(self):
        """insert the data into the db"""
        with sqlite3.connect(self.db_path) as con:
            cur = con.cursor()
            for i, hotel in enumerate(self.hotels):
                try:
                    cur.execute(
                        '''INSERT INTO hotel_metadata (id, name, dist_from_center, hotel_type, address) 
                        VALUES (?,?,?,?,?)''',
                        [
                            hotel.id,
                            hotel.name,
                            hotel.location,
                            hotel.type,
                            hotel.address
                        ])
                    con.commit()
                except sqlite3.IntegrityError:
                    con.rollback()

                cur.execute(
                    "SELECT id, name, hotel_type, address from hotel_metadata"
                )
                all_hotels = cur.fetchall()
                d = {}
                for hotel_scrapped in self.hotels:
                    d[str(hotel_scrapped.id)] = hotel_scrapped

                for hotel_in_db in all_hotels:
                    id = str(hotel_in_db[0])
                    name = hotel_in_db[1]
                    type = hotel_in_db[2]
                    address = hotel_in_db[3]
                    updated_type = type
                    updated_address = address

                    if id in d.keys():
                        if d[id].type:
                            updated_type = d[id].type.strip()
                        if d[id].address:
                            updated_address = d[id].address
                    elif 'hotel' in name.lower():
                        updated_type = 'Hotel'
                    elif 'hostel' in name.lower():
                        updated_type = 'Hostel'
                    elif 'apartments' in name.lower() or 'apartment' in name.lower():
                        updated_type = 'Apartment'
                    elif 'suite' in name.lower():
                        updated_type = 'Suite'
                    if updated_type:
                        cur.execute("UPDATE hotel_metadata SET hotel_type = ? WHERE id = ?", [updated_type, id])
                    if updated_address:
                        cur.execute("UPDATE hotel_metadata SET address = ? WHERE id = ?", [updated_address, id])

            for i, hotel in enumerate(self.hotels):
                cur.execute(
                    '''INSERT INTO pricing (hotel_id, price, score, experience) VALUES (?,?,?,?)''',
                    [
                        hotel.id,
                        hotel.price,
                        hotel.score,
                        hotel.experience
                    ])
                con.commit()

            cur.close()
