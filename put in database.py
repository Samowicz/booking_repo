
import pymysql
from datetime import datetime
import csv
import pandas as pd

USERNAME = 'root'
LOCAL_HOST = 'localhost'
connection = pymysql.connect(host=LOCAL_HOST, user=USERNAME, password='bobo', database='Booking')
cur = connection.cursor()


my_reader = pd.read_csv("pricing_db_sqlite.csv")


for i, row in my_reader.iterrows():
    try:
        score = float(row["score"])
    except:
        score = -1
    try:
        experience = int(row["experience"])
    except:
        experience = 0
    try:
        cur.execute( '''INSERT INTO pricing (hotel_id, price, currency, score, experience, date) VALUES (%s, %s, %s, %s, %s, %s)''',
                        (
                            int(row["hotel_id"]),
                            float(row["price2"]),
                            row["currency"],
                            score,
                            experience,
                            datetime.strptime(row["date"], '%Y-%m-%d %H:%M:%S')
                        ))
    except:
        pass
    connection.commit()
connection.commit()
cur.close()