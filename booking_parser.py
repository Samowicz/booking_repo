from bs4 import BeautifulSoup
import requests

USER_AGENT = 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:47.0) Gecko/20100101 Firefox/48.0'
NAME = "name"
LOCATION = "distfromdest"
SCORE = "score"
EXPERIENCE = "experience"
ID = "id"
PRICE = "price"
IS_AVAILABLE = "is_available"
ADDRESS_URL = "address_url"
ADDRESS = "address"
TYPE = "type"
HOTEL_INFO_PARSER = {
    NAME: 'sr-hotel__name',
    LOCATION: 'distfromdest',
    SCORE: 'bui-review-score__badge',
    EXPERIENCE: 'bui-review-score__text',
    ID: 'data-hotelid',
    ADDRESS_URL: "hotel_name_link url",
    ADDRESS: "hp_address_subtitle",
    TYPE: "hp__hotel-type-badge"
}

class BookingParser:

    def __init__(self, url=None, results_per_page=20, total_pages=1):
        self.hotels = []
        self.url = url
        self.urls = self.get_pages_url(total_pages, results_per_page)

    def get_pages_url(self, num_pages, results_per_page):
        """create the num_pages urls list from the base url"""
        pages = []
        i = 0
        while i < num_pages:
            pages.append(self.url + "&offset={}".format(results_per_page * i))
            i += 1
        return pages

    def get_soup_url(self, url):
        """get the html request from a given url"""
        booking = requests.get(url, headers={'User-Agent': USER_AGENT})
        return BeautifulSoup(booking.content, features="html.parser")

    def get_hotel_blocks(self, soup):
        """gets the div blocks, as a list, containing the hotels information from soup"""
        return soup.find_all('div', class_={'sr_item_new': True})

    def fetch_hotels(self):
        """fetch all the information necessary to build the
        hotels list informations from the urls based on the blocks"""
        for url in self.urls:
            soup = self.get_soup_url(url)
            blocks = self.get_hotel_blocks(soup)
            for block in blocks:
                try:
                    hotelhtmlparser = HotelHtmlParser(block)
                    hotel = hotelhtmlparser.build_hotel()
                    print(hotel.name)
                    self.hotels.append(hotel)
                except ConnectionError:
                    pass
        return self.hotels


class Hotel:
    """create a Hotel class with the following parameters"""
    def __init__(self):
        self.name = ""
        self.price = ""
        self.location = ""
        self.score = ""
        self.experience = ""
        self.id = ""
        self.is_available = True
        self.type = ""
        self.address = ""


class HotelHtmlParser:

    def __init__(self, block):
        """This class gets a block as an argument and fetch each information on the given block"""
        self.block = block

    def fetch(self, key):
        """go fetch the right class based on the key of the HOTEL_INFO_PARSER"""
        info = self.block.find(class_=HOTEL_INFO_PARSER[key])
        if not info:
            return None
        return info.getText().strip("\n")

    def fetch_name(self):
        """fetch the name of the hotel from the block"""
        return self.fetch(NAME)

    def fetch_location(self):
        """fetch the distance in meter from center of city"""
        dist_from_center = self.fetch(LOCATION)
        if not dist_from_center:
            return 0
        dist_from_center = dist_from_center.split(" ")
        distance = dist_from_center[0].replace(",", ".")
        distance = float(distance)
        if dist_from_center[1] == "km":
            distance = distance*1000
        return distance

    def fetch_score(self):
        """fetch the score of the hotel from the block"""
        score = self.fetch(SCORE)
        if not score:
            return 0
        return score.replace(",", ".")

    def fetch_experience(self):
        """fetch the number of "reviews" for each hotel"""
        experience = self.fetch(EXPERIENCE)
        if not experience:
            return 0
        num_exp = ""
        for c in experience:
            if c.isdigit():
                num_exp += c
        return num_exp


    def fetch_id(self):
        """fetch the id of the hotel from the block"""
        return self.block['data-hotelid']

    def fetch_price(self):
        """fetch the price of the hotel from the block"""
        hotel_price = None
        prices = self.block.findAll(True,
                                    {'class': ['price', 'toggle_price_per_night_or_stay', 'bui-price-display__value']})
        for price in prices:
            if price:
                hotel_price = price
                break
        if not hotel_price:
            return None
        return hotel_price.getText().replace(u'\xa0', ' ').strip("\n").strip()

    def fetch_available(self):
        """fetch the availability of the hotel from the block"""
        sold_out = self.block.find('span', class_="sold_out_property")
        if sold_out:
            return False
        else:
            return True


    def fetch_hotel_url(self):
        """go fetch the url of the hotel (in order to fetch the exact address"""
        info_address = self.block.find('a')
        address_url = info_address.get('href')
        if not address_url:
            return None
        return 'https://www.booking.com/' + address_url

    def enter_hotel_url(self):
        """go fetch address' hotel"""
        if self.fetch_hotel_url():
            info_address = requests.get(self.fetch_hotel_url(), headers={'User-Agent': USER_AGENT})
            soup = BeautifulSoup(info_address.content, features="html.parser")
            return soup

    def fetch_address(self):
        soup = self.enter_hotel_url()
        address = soup.find(class_= HOTEL_INFO_PARSER[ADDRESS])
        if not address:
            return "Unknown"
        return address.getText().strip("\n").encode()


    def fetch_type(self):
        soup = self.enter_hotel_url()
        type = soup.find(class_= HOTEL_INFO_PARSER[TYPE])
        if not type:
            return "Unknown"
        return type.getText().strip("\n")

    def build_hotel(self):
        """build the hotel parameters based on the info fetched in the block"""
        hotel = Hotel()
        hotel.id = self.fetch_id()
        hotel.name = self.fetch_name()
        hotel.price = self.fetch_price()
        hotel.location = self.fetch_location()
        hotel.score = self.fetch_score()
        hotel.experience = self.fetch_experience()
        hotel.is_available = self.fetch_available()
        hotel.type = self.fetch_type()
        hotel.address = self.fetch_address()
        hotel.type = self.fetch_type()
        return hotel
