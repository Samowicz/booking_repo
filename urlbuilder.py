
class URLBuilder:
    def __init__(self, config):
        self.url = config['url']
        self.filters = config['filters']

    def build_url(self):
        """build an url with filters from the config file"""
        for filter in self.filters:
            if self.filters[filter]:
                self.url += "&{}={}".format(filter, self.filters[filter])
        return self.url
