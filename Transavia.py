import argparse
import json
from transavia_parser import FlightParser
from transavia_db_mysql import Database


def main():
    parser = argparse.ArgumentParser(description=' Please Process the path to your configuration file. '
                                                 'It has to be a JSON')
    parser.add_argument('path', help='path to the configuration file')
    args = parser.parse_args()
    with open(str(args.path), 'r') as config_file:
        config = json.load(config_file)
    api_response = FlightParser()
    api_response.fetch_response(config['flight_parameters'])
    num_flights = api_response.flight_possibilities()
    if num_flights == 0:
        pass
    else:
        flights = api_response.fetch_flights()
        database = Database(flights, config['name_db'], config['mysql_password'])
        database.create_table()
        database.insert()



if __name__ == '__main__':
    main()


