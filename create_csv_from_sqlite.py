import sqlite3
import pandas as pd
import numpy as np



with sqlite3.connect("booking_database2.db") as con:
    cur = con.cursor()
    pricing = pd.read_sql('''SELECT * FROM pricing''', con)
    cur.close()


def price(row):
    if row["price"]:
        return row["price"][1:].replace(" ","")
    else:
        return np.nan

def currency(row):
    return "NIS"


pricing['price2']=pricing.apply(price, axis=1)
pricing['currency']=pricing.apply(currency, axis=1)

pricing=pricing.drop('price', axis=1)

pricing = pricing.dropna(subset=['price2'], axis=0)

pricing.to_csv("pricing_db_sqlite.csv")