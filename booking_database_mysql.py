import pymysql

USERNAME = 'root'
LOCAL_HOST = 'localhost'

class Database:
    """This class creates a database and insert data into it. Takes hotels and path to db in arguments"""

    def __init__(self, hotels, db_name, mysql_password):
        self.hotels = hotels
        self.db_name = db_name
        self.mysql_password = mysql_password

    def create_database(self):
        """create the database"""
        connection = pymysql.connect(host=LOCAL_HOST, user=USERNAME, password=self.mysql_password)
        cur = connection.cursor()
        try:
            cur.execute("CREATE DATABASE " + self.db_name)
        except pymysql.InternalError:
            pass
        except pymysql.err.ProgrammingError:
            pass
        cur.close()

    def create_table(self):
        """create the tables"""
        connection = pymysql.connect(user=USERNAME, password=self.mysql_password, database=self.db_name)
        cur = connection.cursor()
        try:
            cur.execute('''CREATE TABLE hotel_metadata (
                                    id INT PRIMARY KEY, 
                                    name VARCHAR(200),
                                    dist_from_center INT DEFAULT NULL,
                                    type VARCHAR(200) DEFAULT NULL,
                                    address VARCHAR(200) DEFAULT NULL)''')
        except pymysql.InternalError:
            pass

        try:
            cur.execute('''CREATE TABLE pricing (
                                id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT, 
                                hotel_id INT,
                                price FLOAT DEFAULT NULL,
                                currency VARCHAR(200) DEFAULT NULL,
                                score FLOAT DEFAULT NULL,
                                experience INT DEFAULT NULL,
                                date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
                                FOREIGN KEY (hotel_id) REFERENCES hotel_metadata(id)
                                )''')
        except pymysql.InternalError:
            pass

        cur.close()

    def insert_column(self, table, column):
        connection = pymysql.connect(user=USERNAME, password=self.mysql_password, database=self.db_name)
        cur = connection.cursor()
        try:
            cur.execute(f'''ALTER TABLE {table} ADD COLUMN '{column}' ''')
        except pymysql.InternalError:
            pass
        cur.close()

    def insert(self):
        """insert the data into the db"""
        connection = pymysql.connect(user=USERNAME, password=self.mysql_password, database=self.db_name)
        cur = connection.cursor()
        for hotel in self.hotels:
            try:
                hotel.address.decode('ascii')
            except UnicodeDecodeError:
                hotel.address = "Unknown"
            try:
                cur.execute(
                    '''INSERT INTO hotel_metadata (id, name, dist_from_center, type, address) 
                    VALUES (%s, %s, %s, %s, %s)''',
                    (
                        hotel.id,
                        hotel.name,
                        hotel.location,
                        hotel.type,
                        hotel.address
                    ))
                connection.commit()
            except pymysql.IntegrityError:
                connection.rollback()
            except pymysql.InternalError:
                connection.rollback()


        for hotel in self.hotels:
            currency = None
            if hotel.price:
                if hotel.price[0] == "₪":
                    currency = 'NIS'
                hotel.price = hotel.price[1:].replace(" ", "").replace(",", "")
            else:
                hotel.price = 0
                currency = 'NIS'
            try:
                cur.execute(
                    '''INSERT INTO pricing (hotel_id, price, currency, score, experience) VALUES (%s, %s, %s, %s, %s)''',
                    (
                        hotel.id,
                        int(hotel.price),
                        currency,
                        float(hotel.score),
                        int(hotel.experience)
                    ))
                connection.commit()
            except pymysql.IntegrityError:
                connection.rollback()
            except pymysql.InternalError:
                connection.rollback()

        cur.close()
