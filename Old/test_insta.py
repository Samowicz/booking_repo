import requests
from bs4 import BeautifulSoup
import pandas as pd

#page = requests.get("http://forecast.weather.gov/MapClick.php?lat=37.7772&lon=-122.4168")

#print the response code of the page
#print(page)

#create an instance of the bs class to parse the content of the page in the doc
#soup = BeautifulSoup(page.content, 'html.parser')

#return the HTML content formatted in a nice way
#soup_pretty = soup.prettify()

#print(soup)


#select all the elements at the top level of the file
#soup_children_list = list(soup.children)


#select the html tag and its children which is the 6th item of the list
#html = list(soup_children_list)[6]

#select the body : text inside the p tag
#body = list(html.children)[1]


#get the p tag by finding the children of the body tag
#p_tag = list(body.children)[1]
#p_tag_text = p_tag.get_text()

#to find all the instances of a tag at once
#it returns a list, so to get access to the text you need to put an index
#soup.find_all("p")
#the fin_all returns a list so we'll have to iterate
#soup.find_all("p")[0].get_text()

#find methode will just return the first instance of a tag
#soup.find('p')


#search items by class or id
#print(soup.find_all(id = 'first'))
#print(soup.select("div p"))

page = requests.get("http://forecast.weather.gov/MapClick.php?lat=37.7772&lon=-122.4168")
soup = BeautifulSoup(page.content, 'html.parser')
seven_day = soup.find(id="seven-day-forecast")
forecast_items = seven_day.find_all(class_="tombstone-container")
tonight = forecast_items[0]
#print(tonight.prettify())

period = tonight.find(class_="period-name").get_text()
short_desc = tonight.find(class_="short-desc").get_text()
temp = tonight.find(class_="temp").get_text()
print(period)
print(short_desc)
print(temp)

#img title gives a big description of the weather, it has to be treated like a dict
img = tonight.find("img")
desc = img['title']
print(desc)

period_tags = seven_day.select(".tombstone-container .period-name")
periods = [pt.get_text() for pt in period_tags]
print(periods)

short_descs = [sd.get_text() for sd in seven_day.select(".tombstone-container .short-desc")]
temps = [t.get_text() for t in seven_day.select(".tombstone-container .temp")]
descs = [d["title"] for d in seven_day.select(".tombstone-container img")]
print(short_descs)
print(temps)
print(descs)

weather = pd.DataFrame({
"period": periods,
"short_desc": short_descs,
"temp": temps,
"desc":descs
})
print(weather)



