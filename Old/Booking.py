"""Booking WebScrapping"""

# !/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
from bs4 import BeautifulSoup
import json
import pandas as pd
import requests
import sqlite3
import os



NAME = "name"
LOCATION = "distfromdest"
SCORE = "score"
EXPERIENCE = "experience"
ID = "id"
PRICE = "price"
IS_AVAILABLE = "is_available"
USER_AGENT = 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:47.0) Gecko/20100101 Firefox/48.0'
HOTEL_INFO_PARSER = {
    NAME: 'sr-hotel__name',
    LOCATION: 'distfromdest',
    SCORE: 'bui-review-score__badge',
    EXPERIENCE: 'bui-review-score__text',
    ID: 'data-hotelid'
}


class URLBuilder:
    def __init__(self, config):
        self.url = config['url']
        self.filters = config['filters']

    def build_url(self):
        """build an url with filters from the config file"""
        for filter in self.filters:
            if self.filters[filter]:
                self.url += "&{}={}".format(filter, self.filters[filter])
        return self.url


class BookingParser:

    def __init__(self, url=None, results_per_page=20, total_pages=1):
        self.hotels = []
        self.url = url
        self.urls = self.get_pages_url(total_pages, results_per_page)

    def get_pages_url(self, num_pages, results_per_page):
        """create the num_pages urls list from the base url"""
        pages = []
        i = 0
        while i < num_pages:
            pages.append(self.url + "&offset={}".format(results_per_page * i))
            i += 1
        return pages

    def get_soup_url(self, url):
        """get the html request from a given url"""
        booking = requests.get(url, headers={'User-Agent': USER_AGENT})
        return BeautifulSoup(booking.content, features="html.parser")

    def get_hotel_blocks(self, soup):
        """gets the div blocks, as a list, containing the hotels information from soup"""
        return soup.find_all('div', class_={'sr_item_new': True})

    def fetch_hotels(self):
        """fetch all the information necessary to build the
        hotels list informations from the urls based on the blocks"""
        for url in self.urls:
            soup = self.get_soup_url(url)
            blocks = self.get_hotel_blocks(soup)
            for block in blocks:
                hotelhtmlparser = HotelHtmlParser(block)
                hotel = hotelhtmlparser.build_hotel()
                self.hotels.append(hotel)
        return self.hotels


class Hotel:
    """create a Hotel class with the following parameters"""
    def __init__(self):
        self.name = ""
        self.price = ""
        self.location = ""
        self.score = ""
        self.experience = ""
        self.id = ""
        self.is_available = True


class HotelHtmlParser:

    def __init__(self, block):
        """This class gets a block as an argument and fetch each information on the given block"""
        self.block = block

    def fetch(self, key):
        """go fetch the right class based on the key of the HOTEL_INFO_PARSER"""
        info = self.block.find(class_=HOTEL_INFO_PARSER[key])
        if not info:
            return None
        return info.getText().strip("\n")

    def fetch_name(self):
        """fetch the name of the hotel from the block"""
        return self.fetch(NAME)

    def fetch_location(self):
        """fetch the distance in meter from center of city"""
        dist_from_center = self.fetch(LOCATION)
        if not dist_from_center:
            return None
        dist_from_center = dist_from_center.split(" ")
        distance = dist_from_center[0].replace(",", ".")
        distance = float(distance)
        if dist_from_center[1] == "km":
            distance = distance*1000
        return distance

    def fetch_score(self):
        """fetch the score of the hotel from the block"""
        score = self.fetch(SCORE)
        if not score:
            return None
        return score.replace(",", ".")

    def fetch_experience(self):
        """fetch the number of "reviews" for each hotel"""
        experience = self.fetch(EXPERIENCE)
        if not experience:
            return None
        num_exp = ""
        for c in experience:
            if c.isdigit():
                num_exp += c
        return num_exp


    def fetch_id(self):
        """fetch the id of the hotel from the block"""
        return self.block['data-hotelid']

    def fetch_price(self):
        """fetch the price of the hotel from the block"""
        hotel_price = None
        prices = self.block.findAll(True,
                                    {'class': ['price', 'toggle_price_per_night_or_stay', 'bui-price-display__value']})
        for price in prices:
            if price:
                hotel_price = price
                break
        if not hotel_price:
            return None
        return hotel_price.getText().replace(u'\xa0', ' ').strip("\n").strip()

    def fetch_available(self):
        """fetch the availability of the hotel from the block"""
        sold_out = self.block.find('span', class_="sold_out_property")
        if sold_out:
            return False
        else:
            return True

    def build_hotel(self):
        """build the hotel parameters based on the info fetched in the block"""
        hotel = Hotel()
        hotel.id = self.fetch_id()
        hotel.name = self.fetch_name()
        hotel.price = self.fetch_price()
        hotel.location = self.fetch_location()
        hotel.score = self.fetch_score()
        hotel.experience = self.fetch_experience()
        hotel.is_available = self.fetch_available()
        return hotel


class DataBaseBuilder:
    """This class creates a database and insert data into it. Takes hotels and path to db in arguments"""
    def __init__(self, hotels, db_path):
        self.hotels = hotels
        self.db_path = db_path

    def create_database(self):
        """create the database. If already exists, remove it and recreate it"""
        if os.path.exists(self.db_path):
            os.remove(self.db_path)
        with sqlite3.connect(self.db_path) as con:
            cur = con.cursor()
            cur.execute('''CREATE TABLE hotel_metadata (
                                        id INT PRIMARY KEY, 
                                        name VARCHAR,
                                        dist_from_center INT DEFAULT NULL)''')
            cur.execute('''CREATE TABLE pricing (
                                        id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, 
                                        hotel_id INT,
                                        price VARCHAR DEFAULT NULL,
                                        score FLOAT DEFAULT NULL,
                                        experience INT DEFAULT NULL,
                                        date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
                                        FOREIGN KEY (hotel_id) REFERENCES hotel_metadata(name)
                                        )''')
            con.commit()
            cur.close()

    def insert_in_db(self):
        """insert the data into the db"""
        with sqlite3.connect(self.db_path) as con:
            cur = con.cursor()
            for i, hotel in enumerate(self.hotels):
                try:
                    cur.execute(
                        '''INSERT INTO hotel_metadata (id, name, dist_from_center) VALUES (?,?,?)''',
                        [
                            hotel.id,
                            hotel.name,
                            hotel.location
                        ])
                    cur.execute(
                        '''INSERT INTO pricing (hotel_id, price, score, experience) VALUES (?,?,?,?)''',
                        [
                            hotel.id,
                            hotel.price,
                            hotel.score,
                            hotel.experience
                        ])
                    con.commit()
                except sqlite3.IntegrityError:
                    con.rollback()
            cur.close()


def create_dataframe(hotels):
    """create the df containing all the hotel information"""
    df = pd.DataFrame(columns=[ID, NAME, PRICE, LOCATION, SCORE, EXPERIENCE, ROOM_TYPE, IS_AVAILABLE])
    for i, hotel in enumerate(hotels):
        df.loc[i] = [
            hotel.id,
            hotel.name,
            hotel.price,
            hotel.location,
            hotel.score,
            hotel.experience,
            hotel.room_type,
            hotel.is_available]
    return df


def drop_unwanted_column(df, column_names):
    """drop a given unwanted column from the df"""
    if not column_names:
        return df
    else:
        for name in column_names:
            df = df.drop(name, 1)
    return df


def main():
    parser = argparse.ArgumentParser(description=' Please Process the path to your configuration file. '
                                                 'It has to be a JSON')
    parser.add_argument('path', help='path to the configuration file')
    args = parser.parse_args()
    with open(str(args.path), 'r') as config_file:
        config = json.load(config_file)

    url_builder = URLBuilder(config)
    url_filters = url_builder.build_url()
    booking_parser = BookingParser(url_filters, config['results_per_page'], config['total_pages'])
    hotels = booking_parser.fetch_hotels()
    database = DataBaseBuilder(hotels, config['database_path'])
    database.create_database()
    database.insert_in_db()

    '''df = create_dataframe(hotels)
        df = drop_unwanted_column(df, config['unwanted_columns'])
        df.to_csv(config["dataframe_path"], encoding="utf-8")'''

if __name__ == '__main__':
    main()
