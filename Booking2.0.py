"""Booking WebScrapping"""

# !/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import json
from urlbuilder import URLBuilder
from booking_parser import BookingParser
from booking_database_mysql import Database




def main():
    parser = argparse.ArgumentParser(description=' Pleaase Process the path to your configuration file. '
                                                 'It has to be a JSON')
    parser.add_argument('path', help='path to the configuration file')
    args = parser.parse_args()
    with open(str(args.path), 'r') as config_file:
        config = json.load(config_file)
    url_builder = URLBuilder(config)
    url_filters = url_builder.build_url()
    booking_parser = BookingParser(url_filters, config['results_per_page'], config['total_pages'])
    hotels = booking_parser.fetch_hotels()
    database = Database(hotels, config['name_db'], config['mysql_password'])
    database.create_database()
    database.create_table()
    database.insert()


if __name__ == '__main__':
    main()
